<?php
/**
 * Template Name: Ремонт Окон
 */
get_header();
?>

<section class="v-first-screen" style="background-image: url('<?php echo get_template_directory_uri() ?>/img/v-first-screen-bg.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="v-first-screen__content">
                    <div class="v-first-screen__info">
                        Гарантия на работи - 6 месяцев
                    </div>
                    <h1 class="v-main-title">
                        ремонт и утепление ОКОН <br>
                        <span>с бесплатной</span> диагностикой
                    </h1>
                    <div class="v-first-screen__description">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-warranty.svg" alt="">
                        <p> Гарантия <strong>фиксированной цены</strong>  <br>
                            и высокое качество проводимых работ!
                        </p>
                    </div>
                    <div class="v-button__wrapper">
                        <a href="#" class="v-button modal-form_open">
                            Заказать Бесплатную диагностику
                        </a>
                        <div class="v-button__more-info">
                            <strong>Скидка 10%</strong> - при выполнении
                            работ в день диагностики
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="v-first-screen__img_wrapper">
                    <div class="v-first-screen__img">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-first-screen-window.png" alt="">
                    </div>
                    <div class="v-first-screen__partners-list_wrapper active">
                        <div class="v-first-screen__partners-list_title">
                            Мы работаем с мировыми лидерами:
                        </div>
                        <div class="v-first-screen__partners-list">
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-ok.png" alt="">
                            </div>
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-veka-logo.png" alt="">
                            </div>
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-alias.png" alt="">
                            </div>
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-ok.png" alt="">
                            </div>
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-veka-logo.png" alt="">
                            </div>
                            <div class="v-first-screen__partners-list_img">
                                <img src="<?php echo get_template_directory_uri() ?>/img/v-alias.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="v-first-screen__button">
                        <div class="v-first-screen__button_plus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="v-price">
    <div class="container">
        <div class="v-title">
            <h2>
                Наши цены
            </h2>
            <p>
                Как мы сохраняем тепло и уют у вас дома
            </p>
        </div>
        <div class="v-price__content">
            <div class="v-price__card">
                <h3>
                    Эконом
                </h3>
                <p>
                    Экономия отопления
                </p>
                <div class="v-price__progress v-price__progress_red">
                   <span class="active"></span>
                   <span></span>
                   <span></span>
                   <span></span>
                </div>
                <p>
                    Шумоизоляция
                </p>
                <div class="v-price__progress v-price__progress_blue">
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <p>
                    Солнцезащита
                </p>
                <div class="v-price__progress v-price__progress_yellow">
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="v-price__price">
                    <span>5 700</span> руб
                </div>
                <div class="v-price__new-price">
                    <span>4 450</span> руб
                </div>
                <img src="<?php echo get_template_directory_uri() ?>/img/v-img-whs-halo.png" alt="">

            </div>
            <div class="v-price__card">
                <h3>
                    Эконом
                </h3>
                <p>
                    Экономия отопления
                </p>
                <div class="v-price__progress v-price__progress_red">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                </div>
                <p>
                    Шумоизоляция
                </p>
                <div class="v-price__progress v-price__progress_blue">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                </div>
                <p>
                    Солнцезащита
                </p>
                <div class="v-price__progress v-price__progress_yellow">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="v-price__price">
                    <span>5 700</span> руб
                </div>
                <div class="v-price__new-price">
                    <span>4 450</span> руб
                </div>
                <img src="<?php echo get_template_directory_uri() ?>/img/v-img-whs-halo.png" alt="">

            </div>
            <div class="v-price__card">
                <h3>
                    Эконом
                </h3>
                <p>
                    Экономия отопления
                </p>
                <div class="v-price__progress v-price__progress_red">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                </div>
                <p>
                    Шумоизоляция
                </p>
                <div class="v-price__progress v-price__progress_blue">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                </div>
                <p>
                    Солнцезащита
                </p>
                <div class="v-price__progress v-price__progress_yellow">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span></span>
                </div>
                <div class="v-price__price">
                    <span>5 700</span> руб
                </div>
                <div class="v-price__new-price">
                    <span>4 450</span> руб
                </div>
                <img src="<?php echo get_template_directory_uri() ?>/img/v-img-whs-halo.png" alt="">

            </div>
            <div class="v-price__card">
                <h3>
                    Эконом
                </h3>
                <p>
                    Экономия отопления
                </p>
                <div class="v-price__progress v-price__progress_red">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                </div>
                <p>
                    Шумоизоляция
                </p>
                <div class="v-price__progress v-price__progress_blue">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                </div>
                <p>
                    Солнцезащита
                </p>
                <div class="v-price__progress v-price__progress_yellow">
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                    <span class="active"></span>
                </div>
                <div class="v-price__price">
                    <span>5 700</span> руб
                </div>
                <div class="v-price__new-price">
                    <span>4 450</span> руб
                </div>
                <img src="<?php echo get_template_directory_uri() ?>/img/v-img-whs-halo.png" alt="">

            </div>
        </div>
        <div class="v-button__wrapper v-button__wrapper_center">
            <a href="#" class="v-button question_open">
                получить консультацию
            </a>
        </div>
    </div>
</section>

<section class="v-calculator">
    <div class="container">

        <div class="v-calculator__bg-img">
            <img src="<?php echo get_template_directory_uri() ?>/img/v-calc-bg-img.png" alt="">
        </div>
        <div class="v-title v-title_center v-title_black-block">
            <h2>
                расчет стоимости
            </h2>
        </div>
        <div class="calculator__container price-calculator">
            <div class="price-calculator__title">
                Профильные системы:
            </div>
            <div class="price-calculator__tabs">
                <ul class="price-calculator__tabs-nav">
                    <li>
                        Эконом
                    </li>
                    <li>
                        классик
                    </li>
                    <li>
                        премиум
                    </li>
                    <li>
                        люкс
                    </li>
                </ul>
                <div class="price-calculator__content">
                    <div class="price-calculator__tab">
                        <ul class="price__tabs-nav">
                            <li>
                                Экономия отопления
                            </li>
                            <li>
                                Шумоизоляция
                            </li>
                            <li>
                                Солнцезащита
                            </li>

                        </ul>
                        <div class="price__content">
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый стеклопакет
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 55%">55%</span>
                                                    <progress max="100" value="55"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 55%">55%</span>
                                                    <progress max="100" value="55"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 87%">87%</span>
                                                    <progress max="100" value="87"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>6 300</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>5 500</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина стеклопакета
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 55%">55%</span>
                                                    <progress max="100" value="55"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 47%">47%</span>
                                                    <progress max="100" value="47"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>7 300</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>6 600</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Стеклопакет с УФ отражением
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 55%">55%</span>
                                                    <progress max="100" value="55"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 37%">37%</span>
                                                    <progress max="100" value="37"></progress>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>7 800</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>6 900</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="price-calculator__tab">
                        <ul class="price__tabs-nav">
                            <li>
                                Экономия отопления
                            </li>
                            <li>
                                Шумоизоляция
                            </li>
                            <li>
                                Солнцезащита
                            </li>

                        </ul>
                        <div class="price__content">
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый стеклопакет
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 65%">65%</span>
                                                    <progress max="100" value="65"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 65%">65%</span>
                                                    <progress max="100" value="65"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 87%">87%</span>
                                                    <progress max="100" value="87"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>9 230</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>8 280</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина стеклопакета
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 85%">85%</span>
                                                    <progress max="100" value="85"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 65%">65%</span>
                                                    <progress max="100" value="65"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 57%">57%</span>
                                                    <progress max="100" value="57"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>10 300</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>9 230</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Стеклопакет с УФ отражением
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 65%">65%</span>
                                                    <progress max="100" value="65"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 57%">57%</span>
                                                    <progress max="100" value="57"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>11 200</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>9 600</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="price-calculator__tab">
                        <ul class="price__tabs-nav">
                            <li>
                                Экономия отопления
                            </li>
                            <li>
                                Шумоизоляция
                            </li>
                            <li>
                                Солнцезащита
                            </li>

                        </ul>
                        <div class="price__content">
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый стеклопакет
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 95%">95%</span>
                                                    <progress max="100" value="95"></progress>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>12 300</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>10 500</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина стеклопакета
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 85%">85%</span>
                                                    <progress max="100" value="85"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 85%">85%</span>
                                                    <progress max="100" value="85"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>13 530</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>11 300</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Стеклопакет с УФ отражением
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 87%">87%</span>
                                                    <progress max="100" value="87"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Внутреннее утепление
                                                    </div>

                                                    <span class="progressBar__label"  style="padding-left: 80%">80%</span>
                                                    <progress max="100" value="80"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление откосов
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 85%">85%</span>
                                                    <progress max="100" value="85"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>14 230</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>12 170</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="price-calculator__tab">
                        <ul class="price__tabs-nav">
                            <li>
                                Экономия отопления
                            </li>
                            <li>
                                Шумоизоляция
                            </li>
                            <li>
                                Солнцезащита
                            </li>

                        </ul>
                        <div class="price__content">
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый стеклопакет
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Теплый профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 95%">95%</span>
                                                    <progress max="100" value="95"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Внутреннее утепление
                                                    </div>

                                                    <span class="progressBar__label"  style="padding-left: 80%">80%</span>
                                                    <progress max="100" value="80"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление откосов
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление подоконника
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 90%">90%</span>
                                                    <progress max="100" value="90"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>15 300</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>13 290</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина стеклопакета
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 65%">65%</span>
                                                    <progress max="100" value="65"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Внутреннее утепление
                                                    </div>

                                                    <span class="progressBar__label"  style="padding-left: 60%">60%</span>
                                                    <progress max="100" value="60"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление откосов
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 75%">75%</span>
                                                    <progress max="100" value="75"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление подоконника
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 50%">50%</span>
                                                    <progress max="100" value="50"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>16 530</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>14 250</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                            <div class="price__content_wrapper">
                                <div class="price-calculator__slider">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">

                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Стеклопакет с УФ отражением
                                                    </div>
                                                    <span class="progressBar__label" style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Ширина профиль
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 100%">100%</span>
                                                    <progress max="100" value="100"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Наружное утепление
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 70%">70%</span>
                                                    <progress max="100" value="70"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Внутреннее утепление
                                                    </div>

                                                    <span class="progressBar__label"  style="padding-left: 80%">80%</span>
                                                    <progress max="100" value="80"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление откосов
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 85%">85%</span>
                                                    <progress max="100" value="85"></progress>
                                                </div>
                                            </div>
                                            <div class="swiper-slide">
                                                <div class="progressBar">
                                                    <div class="progressBar__title">
                                                        Утепление подоконника
                                                    </div>
                                                    <span class="progressBar__label"  style="padding-left: 70%">70%</span>
                                                    <progress max="100" value="70"></progress>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-pagination"></div>
                                    </div>
                                    <div class="swiper-button-next"></div>
                                    <div class="swiper-button-prev"></div>
                                </div>
                                <div class="price-calculator__price">
                                    <div class="price-calculator__price_wrapper">
                                        <div class="price-calculator__price_old">
                                            <span>17 230</span> руб
                                        </div>
                                        <div class="price-calculator__price_new">
                                            <span>15 270</span> руб
                                        </div>
                                    </div>
                                    <div class="v-button">
                                        оставить заявку и получить скидку
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="v-price-windows">
    <div class="container">
        <div class="v-title v-title_center v-title_black-block">
            <h2>
                Цены
            </h2>
            <p>
                на пластиковые окна:
            </p>
        </div>
        <div class="v-price-windows__content">
            <div class="v-price-windows__card">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-window-1.jpg" alt="">

                <div class="v-price-windows__price">
                    <strong>5 700</strong> руб
                </div>

                <h3>
                    Одностворчатое окно
                </h3>
                <span>
                    Поворотно - откидная створка
                </span>
                <a href="#" class="v-button">
                    бесплатный замер
                </a>

            </div>
            <div class="v-price-windows__card">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-window-2.jpg" alt="">

                <div class="v-price-windows__price">
                    <strong>7 450</strong> руб
                </div>

                <h3>
                    Двухстворчатое окно
                </h3>
                <span>
                    Поворотно - откидная
                    + глухая створка
                </span>

                <a href="#" class="v-button">
                    бесплатный замер
                </a>

            </div>
            <div class="v-price-windows__card">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-window-3.jpg" alt="">

                <div class="v-price-windows__price">
                    <strong>9 450</strong> руб
                </div>

                <h3>
                    Трехстворчатое окно
                </h3>
                <span>
                    Поворотно - откидная
                    + 2 глухие створки
                </span>

                <a href="#" class="v-button">
                    бесплатный замер
                </a>

            </div>
            <div class="v-price-windows__card">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-window-4.jpg" alt="">

                <div class="v-price-windows__price">
                    <strong>14 450</strong> руб
                </div>

                <h3>
                    Балконный блок с дверью
                </h3>
                <span>
                    Дверь + 2 глухие створки
                </span>

                <a href="#" class="v-button">
                    бесплатный замер
                </a>

            </div>
        </div>
    </div>
</section>

<section class="v-stocks">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-stocks.jpg" alt="">
            </div>
            <div class="col-lg-4">
                <div class="v-stocks__content">
                    <div class="v-title v-title_bottom-line">
                        <h2>
                            акция
                        </h2>
                    </div>
                    <div class="v-stocks__info">
                        <span class="v-stocks__info_size-stock">
                            10%
                        </span>
                        <span class="v-stocks__info_about-stock">
                            Скидки пенсионерам <br> и молодим семьям
                        </span>
                    </div>
                    <ul class="v-stocks__list">
                        <li>
                            роль шторы в подарок
                        </li>
                    </ul>
                    <div class="v-button__wrapper v-button__wrapper_mobile-center">
                        <a href="#" class="v-button akciya_open">
                            узнать условия акции
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="v-our-work v-reviews">
    <div class="container">
        <div class="v-title v-title_center">
            <h2>
                Что говорят клиенты
            </h2>
            <p>
                о нашей работе
            </p>
        </div>
        <div class="v-our-work__slider">
            <div class="v-our-work__slide">
                <a data-fancybox="v-reviews" href="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg" alt="">
                </a>
                <h3>
                    Теперь понастоящему тепло
                </h3>
                <p>
                    в нашем доме
                </p>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-reviews" href="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg" alt="">
                </a>
                <h3>
                    Теперь понастоящему тепло
                </h3>
                <p>
                    в нашем доме
                </p>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-reviews" href="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg" alt="">
                </a>
                <h3>
                    Теперь понастоящему тепло
                </h3>
                <p>
                    в нашем доме
                </p>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-reviews" href="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-rewiev-1.jpg" alt="">
                </a>
                <h3>
                    Теперь понастоящему тепло
                </h3>
                <p>
                    в нашем доме
                </p>
            </div>


        </div>
    </div>
</section>

<section class="v-specialists">
    <div class="container">
        <div class="v-title v-title_black-block v-title_center">
            <h2>
                наши специалисты
            </h2>
            <p>
                каждый из нас - специалист с большим опытом работы
            </p>
        </div>
        <div class="v-specialists__slider">
            <div class="slider-nav">
                <div class="slide">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-1-small.jpg" alt="">
                    <h3>
                        Дмитрий Иванов
                    </h3>
                    <p>
                        Директор
                    </p>
                </div>
                <div class="slide">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-1-small.jpg" alt="">
                    <h3>
                        Станислав Плешко
                    </h3>
                    <p>
                        Соосновmmатель
                    </p>
                </div>
                <div class="slide">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-1-small.jpg" alt="">
                    <h3>
                        Дмитрий Иванов
                    </h3>
                    <p>
                        Директор
                    </p>
                </div>
                <div class="slide">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-1-small.jpg" alt="">
                    <h3>
                        Дмитрий Данилов
                    </h3>
                    <p>
                        Управляющий
                    </p>
                </div>
            </div>
            <div class="slider-for">
                <div class="slide-for">
                    <div class="v-specialists__card">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-card-1.jpg" alt="">
                        <div class="v-specialists__info">
                            <h3>
                                Дмитрий Иванов
                            </h3>
                            <h4>
                                Директор компании “Стоп Сквозняк”
                            </h4>
                            <ul>
                                <li>
                                    <strong>Стаж роботи:</strong> 10 лет
                                </li>
                            </ul>
                            <p>
                                Чтобы хорошо жить, нужно много работать. А для того, чтобы стать богатым, нужно придумать что-то другое.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="slide-for">
                    <div class="v-specialists__card">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-card-1.jpg" alt="">
                        <div class="v-specialists__info">
                            <h3>
                                Дмитрий Иванов
                            </h3>
                            <h4>
                                Директор компании “Стоп Сквозняк”
                            </h4>
                            <ul>
                                <li>
                                    <strong>Стаж роботи:</strong> 10 лет
                                </li>
                            </ul>
                            <p>
                                Чтобы хорошо жить, нужно много работать. А для того, чтобы стать богатым, нужно придумать что-то другое.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="slide-for">
                    <div class="v-specialists__card">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-card-1.jpg" alt="">
                        <div class="v-specialists__info">
                            <h3>
                                Дмитрий Иванов
                            </h3>
                            <h4>
                                Директор компании “Стоп Сквозняк”
                            </h4>
                            <ul>
                                <li>
                                    <strong>Стаж роботи:</strong> 10 лет
                                </li>
                            </ul>
                            <p>
                                Чтобы хорошо жить, нужно много работать. А для того, чтобы стать богатым, нужно придумать что-то другое.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="slide-for">
                    <div class="v-specialists__card">
                        <img src="<?php echo get_template_directory_uri() ?>/img/v-specialist-card-1.jpg" alt="">
                        <div class="v-specialists__info">
                            <h3>
                                Дмитрий Иванов
                            </h3>
                            <h4>
                                Директор компании “Стоп Сквозняк”
                            </h4>
                            <ul>
                                <li>
                                    <strong>Стаж роботи:</strong> 10 лет
                                </li>
                            </ul>
                            <p>
                                Чтобы хорошо жить, нужно много работать. А для того, чтобы стать богатым, нужно придумать что-то другое.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="v-our-work">
    <div class="container">
        <div class="v-title v-title_center">
            <h2>
                наши работы
            </h2>
        </div>
        <div class="v-our-work__slider">
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
            <div class="v-our-work__slide">
                <a data-fancybox="v-gallery" href="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg">
                    <img src="<?php echo get_template_directory_uri() ?>/img/v-our-work-slider-1.jpg" alt="">
                </a>
            </div>
        </div>
    </div>
</section>

<section class="v-stocks v-installment-plan">
    <div class="container">
        <div class="row d-flex flex-row-reverse">
            <div class="col-lg-8">
                <img src="<?php echo get_template_directory_uri() ?>/img/v-stocks.jpg" alt="">
            </div>
            <div class="col-lg-4">
                <div class="v-stocks__content">
                    <div class="v-title v-title_bottom-line">
                        <h2>
                            рассрочка
                        </h2>
                        <p>
                            на окна
                        </p>
                    </div>
                    <p>
                        Заберите свое окно прямо сейчас!
                    </p>
                    <div class="v-stocks__info">
                        <span class="v-stocks__info_size-stock">
                            0%
                        </span>
                        <span class="v-stocks__info_about-stock">
                            А оплатите <br> за него позже!
                        </span>
                    </div>

                    <div class="v-button__wrapper v-button__wrapper_mobile-center">
                        <a href="#" class="v-button akciya_open">
                            оставить заявку
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer('repair'); ?>
